# -*- coding: latin1 -*-
#!usr/bin/env/ python
from escpos import printer
import sys, json
Epson = printer.Usb(0x04b8,0x0e15)
for line in sys.stdin:
	data = json.loads(line)
	cut = json.loads(data['data'])
	sellpoint = json.loads(data['sellpoint'])
	Epson.set("center", "A", "B", 1, 2)
	Epson.text("ID " + data['num'] + "\n")
	Epson.text("TOTAL " + str(format(cut['total']+cut['pen'], ',.2f')) + "\n")
	Epson.text("\n")
	Epson.text("\n")
	Epson.set("center","A","B",1,2)
	Epson.text("TOTAL sin pendientes " + str(format(cut['total'], ',.2f')) + "\n")
	Epson.text("COBRADOS " + str(cut['cob']) + "\n")
	Epson.text("FALTANTES " + str(cut['pen'])+ "\n")
	Epson.text("TOTAL FALTANTE " + str(format(data['pen'], ',.2f')) + "\n")
	Epson.text("FECHA " + cut['format_final_time'] + "\n")
	Epson.text("\n")
	Epson.text("\n")
	Epson.set("center","A","B",1,2)
	if sellpoint['header_line_black_1']:
		Epson.text(sellpoint['header_line_black_1'].encode("latin1", "surrogateescape").decode("latin1"))
		Epson.text("\n")
	if sellpoint['header_line_black_2']:
		Epson.text(sellpoint['header_line_black_2'].encode("latin1", "surrogateescape").decode("latin1"))
		Epson.text("\n")
	if sellpoint['header_line_black_3']:
		Epson.text(sellpoint['header_line_black_3'].encode("latin1", "surrogateescape").decode("latin1"))
		Epson.text("\n")
	Epson.set("center","A","normal",1,1)
	if sellpoint['header_line_1']:
		Epson.text(sellpoint['header_line_1'].encode("latin1", "surrogateescape").decode("latin1"))
		Epson.text("\n")
	if sellpoint['header_line_2']:
		Epson.text(sellpoint['header_line_2'].encode("latin1", "surrogateescape").decode("latin1"))
		Epson.text("\n")
	if sellpoint['header_line_3']:
		Epson.text(sellpoint['header_line_3'].encode("latin1", "surrogateescape").decode("latin1"))
		Epson.text("\n")
	if sellpoint['header_line_4']:
		Epson.text(sellpoint['header_line_4'].encode("latin1", "surrogateescape").decode("latin1"))
		Epson.text("\n")
	if sellpoint['header_line_5']:
		Epson.text(sellpoint['header_line_5'].encode("latin1", "surrogateescape").decode("latin1"))
		Epson.text("\n")
	Epson.text("\n")
	Epson.text("\n")
	Epson.set("center", "A", "normal", 1, 1)
	Epson.text(data['l'])
	Epson.set("center","A","B",1,2)
	Epson.text("\n")
	Epson.text("\n")
	Epson.text("\n")
	Epson.text("IVA " + str(format(json.loads(data['iva']), ',.2f')) + "\n")
	Epson.text("IEPS " + str(format(json.loads(data['ieps']), ',.2f')) + "\n")
	Epson.text("SUBTOTAL " + str( format( cut['total'] + cut['pen'] - json.loads(data['iva']) - json.loads(data['ieps'])  , ',.2f')) + "\n")
	Epson.text("TOTAL " + str(format(cut['total']+cut['pen'], ',.2f')) + "\n")
	Epson.text("\n")
	Epson.text("\n")
	Epson.text("PAN BLANCO " + str(json.loads(data['blanco_pz'])) + 'pz $' + format(json.loads(data['blanco']), ',.2f') + "\n")
	Epson.text("PAN DULCE " + str(json.loads(data['dulce_pz'])) + 'pz $' + format(json.loads(data['dulce']), ',.2f') + "\n")
	Epson.text("OTROS " + str(json.loads(data['otros_pz'])) + 'pz $' + format(json.loads(data['otros']), ',.2f') + "\n")
	Epson.cut()