var io = require('socket.io')(8000);
io.on('connection', function (socket) {
    socket.on('room', function (data) {
        socket.join(data);
    })
    socket.on('emit_ticket', function (data) {
        io.sockets.in(data.company).emit('show_ticket', data);
    });
    socket.on('print_ticket', function (data) {
        io.sockets.in(data.company).emit('cordova_ticket', data);
    });
    socket.on('print_cut', function (data) {
        io.sockets.in(data.company).emit('cordova_cut', data);
    });
});