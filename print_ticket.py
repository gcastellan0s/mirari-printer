# -*- coding: latin1 -*-
#!usr/bin/env/ python
from escpos import printer
import sys, json
Epson = printer.Usb(0x04b8,0x0e15)
for line in sys.stdin:
        data = json.loads(line)
        for i in range(data['number']):
                ticket = json.loads(data['ticket'])
                sellpoint = json.loads(data['sellpoint'])
                Epson.set("center","A","B",2,2)
                Epson.qr(ticket['barcode']+','+str(ticket['total'])+','+ticket['secret_code']+','+ticket['format_date']+','+ticket['format_time'], size=8)
                Epson.text( "\n")
                if 'order_tocount' in  ticket:
                        if ticket['order_tocount']:
                                Epson.text("A CUENTA " + str(format( float(ticket['order_tocount']) , ',.2f')) + "\n")
                                Epson.set("center","A","B",1,1)
                                Epson.text("RESTANTE " + str(format( float(ticket['total']) - float(ticket['order_tocount']) , ',.2f')) + "\n")
                                Epson.text("CLIENTE " + ticket['order_name'] + "\n")
                                Epson.text("CONTACTO " + ticket['order_phone'] + "\n")
                                Epson.text("FECHA " + ticket['order_datetime'] + "\n")
                                Epson.text("NOTAS " + ticket['order_notes'] + "\n")
                                Epson.set("center","A","B",2,2)
                                Epson.text( "------------------------\n")
                Epson.text("TOTAL " + str(format(ticket['total'], ',.2f')) + "\n")
                Epson.set("center","A","B",1,2)
                if sellpoint['header_line_black_1']:
                        Epson.text(sellpoint['header_line_black_1'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_black_2']:
                        Epson.text(sellpoint['header_line_black_2'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_black_3']:
                        Epson.text(sellpoint['header_line_black_3'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                Epson.set("center","A","normal",1,1)
                if sellpoint['header_line_1']:
                        Epson.text(sellpoint['header_line_1'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_2']:
                        Epson.text(sellpoint['header_line_2'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_3']:
                        Epson.text(sellpoint['header_line_3'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_4']:
                        Epson.text(sellpoint['header_line_4'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_5']:
                        Epson.text(sellpoint['header_line_5'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                Epson.text(ticket['barcode'].encode("latin1", "surrogateescape").decode("latin1"))
                Epson.text("\n")
                if ticket['status'] == 'COB':
                        Epson.text("::PAGADO::\n")
                Epson.text( "----------------------------\n")
                for lines in ticket['ticket_lines']:
                        Epson.set("center","A","normal",1,1)
                        Epson.text( str(lines['quantity']) + 'u. de ')
                        Epson.text( lines['alias'] + ' a $' + str(format(lines['total'], ',.2f'))  + " > ")
                        Epson.set("center","A","B",1,1)
                        Epson.text( "$" +str(format(lines['total'], ',.2f'))  + "\n")
                Epson.set("center","A","normal",1,1)
                Epson.text( "----------------------------\n")
                Epson.set("right","A","normal",1,1)
                if ticket['iva'] or ticket['ieps']:
                        Epson.text( "Subtotal: " + str(format(ticket['subtotal'], ',.2f')) + "          \n")
                if ticket['iva']:
                        Epson.text( "IVA: " + str(format(ticket['iva'], ',.2f')) + "          \n")
                if ticket['ieps']:
                        Epson.text( "IEPS: " + str(format(ticket['ieps'], ',.2f')) + "          \n")
                Epson.text( "TOTAL: " + str(format(ticket['total'], ',.2f')) + "          \n")
                Epson.set("center","A","normal",1,1)
                Epson.text( "----------------------------\n")
                if sellpoint['footer_line_1']:
                        Epson.text(sellpoint['footer_line_1'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['footer_line_2']:
                        Epson.text(sellpoint['footer_line_2'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['footer_line_3']:
                        Epson.text(sellpoint['footer_line_3'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                Epson.cut()
        for i in range(data['number']):
                ticket = json.loads(data['ticket'])
                sellpoint = json.loads(data['sellpoint'])
                Epson.set("center","A","B",2,2)
                Epson.qr(ticket['barcode']+','+str(ticket['total'])+','+ticket['secret_code']+','+ticket['format_date']+','+ticket['format_time'], size=8)
                Epson.text( "\n")
                if 'order_tocount' in  ticket:
                        if ticket['order_tocount']:
                                Epson.text("A CUENTA " + str(format( float(ticket['order_tocount']) , ',.2f')) + "\n")
                                Epson.set("center","A","B",1,1)
                                Epson.text("RESTANTE " + str(format( float(ticket['total']) - float(ticket['order_tocount']) , ',.2f')) + "\n")
                                Epson.text("CLIENTE " + ticket['order_name'] + "\n")
                                Epson.text("CONTACTO " + ticket['order_phone'] + "\n")
                                Epson.text("FECHA " + ticket['order_datetime'] + "\n")
                                Epson.text("NOTAS " + ticket['order_notes'] + "\n")
                                Epson.set("center","A","B",2,2)
                                Epson.text( "------------------------\n")
                Epson.text("TOTAL " + str(format(ticket['total'], ',.2f')) + "\n")
                Epson.set("center","A","B",1,2)
                if sellpoint['header_line_black_1']:
                        Epson.text(sellpoint['header_line_black_1'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_black_2']:
                        Epson.text(sellpoint['header_line_black_2'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_black_3']:
                        Epson.text(sellpoint['header_line_black_3'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                Epson.set("center","A","normal",1,1)
                if sellpoint['header_line_1']:
                        Epson.text(sellpoint['header_line_1'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_2']:
                        Epson.text(sellpoint['header_line_2'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_3']:
                        Epson.text(sellpoint['header_line_3'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_4']:
                        Epson.text(sellpoint['header_line_4'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['header_line_5']:
                        Epson.text(sellpoint['header_line_5'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                Epson.text(ticket['barcode'].encode("latin1", "surrogateescape").decode("latin1"))
                Epson.text("\n")
                if ticket['status'] == 'COB':
                        Epson.text("::PAGADO::\n")
                Epson.text( "----------------------------\n")
                for lines in ticket['ticket_lines']:
                        Epson.set("center","A","normal",1,1)
                        Epson.text( str(lines['quantity']) + 'u. de ')
                        Epson.text( lines['alias'] + ' a $' + str(format(lines['total'], ',.2f'))  + " > ")
                        Epson.set("center","A","B",1,1)
                        Epson.text( "$" +str(format(lines['total'], ',.2f'))  + "\n")
                Epson.set("center","A","normal",1,1)
                Epson.text( "----------------------------\n")
                Epson.set("right","A","normal",1,1)
                if ticket['iva'] or ticket['ieps']:
                        Epson.text( "Subtotal: " + str(format(ticket['subtotal'], ',.2f')) + "          \n")
                if ticket['iva']:
                        Epson.text( "IVA: " + str(format(ticket['iva'], ',.2f')) + "          \n")
                if ticket['ieps']:
                        Epson.text( "IEPS: " + str(format(ticket['ieps'], ',.2f')) + "          \n")
                Epson.text( "TOTAL: " + str(format(ticket['total'], ',.2f')) + "          \n")
                Epson.set("center","A","normal",1,1)
                Epson.text( "----------------------------\n")
                if sellpoint['footer_line_1']:
                        Epson.text(sellpoint['footer_line_1'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['footer_line_2']:
                        Epson.text(sellpoint['footer_line_2'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                if sellpoint['footer_line_3']:
                        Epson.text(sellpoint['footer_line_3'].encode("latin1", "surrogateescape").decode("latin1"))
                        Epson.text("\n")
                Epson.cut()